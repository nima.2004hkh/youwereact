# YouWe React Assessment

SSR and functional component with hooks ( react version +16.8 ,jss , webpack , express )

#### features added

- ssr provider supporting
- hot reloading
- jss support
- jest and enzyme test

#### step 1

```
git clone https://gitlab.com/nima.2004hkh/youwereact.git
```

#### step 2

```
yarn install  && yarn global add webpack
```

or in npm :

```
npm i && npm -g i webpack
```

#### step 3

run production environment

```
npm run build:prod && npm run start:prod
```

or you can use it as development environment

```
npm run build:dev
```
then start Development mod

```
npm run start:dev
```

for test unites

```
npm run test
```
and for webpack analyze :

```
npm run analyze
```
this project has been created with  :
https://www.npmjs.com/package/react-easy-boilerplate
that made by me.
