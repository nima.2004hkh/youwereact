const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const distDir = path.join(__dirname, '../dist');
const srcDir = path.join(__dirname, '../src');

module.exports = [
  {
    mode: 'development',
    name: 'client',
    target: 'web',
    entry: `${srcDir}/client.js`,
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'client.js',
      publicPath: '/dist/'
    },
    resolve: {
      extensions: [
        '.js', '.jsx'
      ],
      alias: {
        'react-dom': '@hot-loader/react-dom'
      }
    },
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif|svg)$/i,
          use: [
            {
              loader: 'file-loader'
            }
          ]
        }, {
          test: /\.ttf$/,
          use: [
            {
              loader: 'ttf-loader',
              options: {
                name: './assests/fonts/[hash].[ext]'
              }
            }
          ]
        }, {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules[\\\/])/,
          use: [
            {
              loader: 'babel-loader'
            }
          ]
        }, {
          test: /\.pcss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  modules: true,
                  importLoaders: 1,
                  localIdentName: '[local]',
                  sourceMap: true
                }
              }, {
                loader: 'postcss-loader',
                options: {
                  config: {
                    path: `${__dirname}/../postcss/postcss.config.js`
                  }
                }
              }
            ]
          })
        }
      ]
    },
    plugins: [
      new BundleAnalyzerPlugin(),
      new ExtractTextPlugin({filename: 'styles.css', allChunks: true})
    ]
  }, {
    name: 'server',
    target: 'node',
    entry: `${srcDir}/server.js`,
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'server.js',
      libraryTarget: 'commonjs2',
      publicPath: '/dist/'
    },
    resolve: {
      extensions: ['.js', '.jsx']
    },
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif|svg)$/i,
          use: [
            {
              loader: 'file-loader'
            }
          ]
        }, {
          test: /\.ttf$/,
          use: [
            {
              loader: 'ttf-loader',
              options: {
                name: './assests/fonts/[hash].[ext]'
              }
            }
          ]
        }, {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules[\\\/])/,
          use: [
            {
              loader: 'babel-loader'
            }
          ]
        }, {
          test: /\.pcss$/,
          use: [
            {
              loader: 'isomorphic-style-loader'
            }, {
              loader: 'css-loader',
              options: {
                modules: true,
                importLoaders: 1,
                localIdentName: '[local]',
                sourceMap: false
              }
            }, {
              loader: 'postcss-loader',
              options: {
                config: {
                  path: `${__dirname}/../postcss/postcss.config.js`
                }
              }
            }
          ]
        }
      ]
    }
  }
];
