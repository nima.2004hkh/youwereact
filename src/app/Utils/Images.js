import Bitmap from "../assets/images/Bitmap.png"
import Zoeken from "../assets/images/zoeken.png"
import OpenSans from "../assets/fonts/OpenSans-Regular.ttf"
import OpenSansBold from "../assets/fonts/OpenSans-Bold.ttf"
import ArrowRightSecondary from "../assets/images/arrow-right copy 2.png"
import ArrowRight from "../assets/images/arrow-right.png"
import accordionShape from "../assets/images/Shape 2.png"
import addImage from "../assets/images/add.png"
import newsPaper from "../assets/images/newspaper.png"
import Calendar from "../assets/images/calendar.png"
import ExternalLink from "../assets/images/external-link.png"
import Shape from "../assets/images/Shape.png"
import Kwaliteitshandboek from "../assets/images/kwaliteitshandboek.png"
import Groups from "../assets/images/groups.png"
import phonebook from "../assets/images/telephone.png"
import BlackSettingButton from "../assets/images/black-settings-button.svg"
import NotificationBell from "../assets/images/notification-bell.svg"
import toolsButton from "../assets/images/Bitmap 2.png"
import Oval from "../assets/images/Oval 2 Copy 2.png"
import Oval1 from "../assets/images/Oval 2.png"
import Oval22 from "../assets/images/Oval 22.png"
import likeComment from "../assets/images/Bitmap 4.png"
import unNamed from "../assets/images/unnamed.png"
import Item1 from "../assets/images/images.png"
import Avatar from "../assets/images/Oval 2 2.png"
import Bitmap3 from "../assets/images/Bitmap 3.png"
import Comment from "../assets/images/comment.png"
import Rectangle from "../assets/images/Rectangle 5.png"
import Rectangle5 from "../assets/images/Rectangle 5 Copy.png"
import Rectangle6 from "../assets/images/Rectangle 5 Copy 2.png"

export {
  toolsButton,
  Rectangle,
  Rectangle5,
  Rectangle6,
  Item1,
  Bitmap,
  Avatar,
  Bitmap3,
  Comment,
  Oval,
  Oval1,
  Oval22,
  unNamed,
  likeComment,
  Zoeken,
  OpenSans,
  OpenSansBold,
  ArrowRightSecondary,
  ArrowRight,
  accordionShape,
  addImage,
  newsPaper,
  Calendar,
  ExternalLink,
  Shape,
  Kwaliteitshandboek,
  Groups,
  phonebook,
  BlackSettingButton,
  NotificationBell
}
