import React,{useState} from 'react'

export function useAccordion(){
  const [openAccordion, setOpenAccordion]=useState(true);

  function changeOpen(status){

    let stat=false;
    if(status!==undefined){
      stat=status;
    }
    else{
      stat=!openAccordion
    }
    setOpenAccordion(stat);
  }

  return [openAccordion, changeOpen];

}
