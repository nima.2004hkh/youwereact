import React from "react"
import {NotificationBell,BlackSettingButton} from "../Utils/Images"

export default function Header(props){
  const {classes} = props;

  return(
    <React.Fragment>
      <div className={classes.fullWidth}>
        <div className={classes.appbar}>
          <div id="appbar_wrapper">
            <div id="left_menu">
              <ul className={[classes.top_menu].join(" ")}>
                  <li>
                    <a href="#Groepen">Groepen</a>
                  </li>
                  <li>
                    <a href="#Contact">Contact</a>
                  </li>
                  <li>
                    <a href="#FAQ">FAQ</a>
                  </li>
                  <li>
                    <a href="#Smoelenboek">Smoelenboek</a>
                  </li>
                  <li>
                    <a href="#Microblog">Microblog</a>
                  </li>
                  <li>
                    <a href="#Overons">Over ons</a>
                  </li>
                </ul>
            </div>
            <div id="settings">
              <ul className={classes.nav} id="right_menu">
                <img className={classes.buttons} src={BlackSettingButton}/>
                <img className={classes.buttons} src={NotificationBell}/>
              </ul>
            </div>
          </div>
        </div>
        <div className={classes.logo}>
          <div className={classes.image}></div>
          <div className={classes.slogan}>
            <span id="site_name">Cura</span>
            <span id="site_name_part_two">Nu</span>
            <span id="slogan_text">
              Zorg en welzijn
              <span id="slogan_bold">Intranet</span>
            </span>
          </div>

        </div>
      </div>
      <div className={['col',classes.navigation].join(" ")}>
        <div className="col8">
          <ul className={classes.nav}>
            <li>
              <a href="#Artikelen">Artikelen</a>
            </li>
            <li>
              <a href="#Nieuws">Nieuws</a>
            </li>
            <li>
              <a href="#Evenementen">Evenementen</a>
            </li>
            <li>
              <a href="#Kwaliteitshandboek">Kwaliteitshandboek</a>
            </li>
          </ul>
        </div>
        <div className="col4">
          <div className={classes.search}>
            <input type="text" className={classes.search_input}/>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}
