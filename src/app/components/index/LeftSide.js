import React from "react"
import {Accordions, Evenementen, Nieuws, Widget} from "./LeftBlocks"

export default function LeftSide(props) {
  const {classes} = props;
  const BlocksNames = ['Accordions', 'Evenementen', 'Nieuws', 'Widget'];
  const Blocks = {
    Accordions: Accordions,
    Evenementen: Evenementen,
    Nieuws: Nieuws,
    Widget: Widget
  };

  return (<div className={[classes.column, classes.block].join(" ")}>
    {
      BlocksNames.map(component => {
        const Handler = Blocks[component];
        return (<Handler classes={classes} key={component}/>)
      })
    }
  </div>)
}
