import React from "react"
import {Telefoonboek, Microblog, MijnGroepen} from "./RightBlocks"

export default function LeftSide(props) {
  const {classes} = props;
  const BlocksNames = ['Telefoonboek', 'Microblog', 'MijnGroepen'];
  const Blocks = {
    Telefoonboek: Telefoonboek,
    Microblog: Microblog,
    MijnGroepen: MijnGroepen
  };

  return (<div className={[classes.column,classes.block].join(" ")}>
    {
      BlocksNames.map(component =>{
        const Handler = Blocks[component];
        return (<Handler classes={classes} key={component}/>)
      })
    }
  </div>)
}
