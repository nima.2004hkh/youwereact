import React, {useState, useEffect} from "react"
import {
  toolsButton,
  likeComment,
  Oval,
  Oval1,
  Oval22,
  unNamed
} from "../../../Utils/Images"

import {useAccordion} from "../../../Utils/Hooks"

/**
 * Telefoonboek
*/
function Telefoonboek(props) {
  const {classes} = props

  return (<div className={classes.phoneBook}>
    <h3 className={classes.title}>
      Telefoonboek
    </h3>
    <div className="text-white margin-top">
      Vind collega’s op naam, trefwoord, functie, etc.
    </div>
    <div className={classes.search}>
      <input type="text" className={classes.search_input}/>
    </div>
  </div>)
}

/**
 * Microblog
*/
function Microblog(props) {
  const [open, dispatch] = useAccordion();
  const [item, setItem] = useState(1);
  const {classes} = props

  function handleAccordion(stat, item) {
    dispatch(stat);
    setItem(item)
  }

  return (<dl onClick={() => handleAccordion(!open, open?0:1)} className={[classes.accordion ,classes.collapsable].join(" ")}>
    <dt className={[item===1 ? classes.expanded : "" ,classes.close].join(" ")}>
      <span className="icon_edit"></span>microblog</dt>
    <dd>
      <div className={classes.microblog}>
        <form noValidate="novalidate" onSubmit={(event) => {
            event.preventDefault();
            return false
          }}>
          <textarea cols="10" rows="7"></textarea>
          <div className="tools">
            <div className="plaats">
              <button>Plaats</button>
            </div>
            <div className={classes.tools_buttons}>
              <img src={toolsButton}/>
            </div>
          </div>
        </form>

        <div className="col margin-top--xxl">
          <div className="col3">
            <img src={Oval1} className={classes.avatar}/>
          </div>
          <div className="col7">
            <span className={classes.author}>Ria de Vries</span>
            <br/>
            <span className={classes.date}>12/09/2016 - 11:10</span>
          </div>
          <div className={['col2', classes.like_comment].join(" ")}>
            <div className="col">
              <img src={likeComment}/>
            </div>
          </div>
        </div>
        <div className={classes.blog_article}>
          <div className={classes.blog_content}>
            Nieuwe campagne CuraNed gisteren van start gegaan
          </div>
          <div>
            <img src={unNamed}/>
          </div>
          <div className={classes.under_content}>
            Lees meer en reacties (0)
          </div>
        </div>

      </div>
    </dd>

    <dt className={classes.meer}>Toon Meer</dt>
    <dd></dd>
  </dl>)
}

/**
 * Mijn groepen
*/
function MijnGroepen(props) {
  const [open, dispatch] = useAccordion();
  const [item, setItem] = useState(1);
  const {classes} = props

  function handleAccordion(stat, item) {
    dispatch(stat);
    setItem(item)
  }

  return (<dl onClick={() => handleAccordion(!open, open?0:1)} className={[classes.accordion, classes.collapsable].join(" ")} >
    <dt className={[item===1 ? classes.expanded : "" , 'close'].join(" ")}>
      <span className="icon_groups"></span>Mijn groepen</dt>
    <dd>
      <div className={[classes.direct, classes.groups].join(" ")}>
        <div className={classes.item}>
          <div className="col3">
            <img src={Oval22} title="item1"/>
          </div>
          <div className="col9">
            <span>HRM</span>
          </div>
        </div>

        <div className={classes.item}>
          <div className="col3">
            <img src={Oval} title="item2"/>
          </div>
          <div className="col9">
            <span>Marketing & Communicatie</span>
          </div>
        </div>

      </div>
    </dd>

    <dt className={classes.meer}>Meer groepen</dt>
    <dd></dd>
  </dl>)
}

export {
  Telefoonboek,
  Microblog,
  MijnGroepen
}
