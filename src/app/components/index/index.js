import React, {useState} from 'react';
import injectSheet from 'react-jss';
import {NavLink} from 'react-router-dom';
import Header from "../Header"
import LeftSide from "./LeftSide"
import Center from "./Center"
import RightSide from "./RightSide"
import {index as styles} from "../../styles"

const Homepage = injectSheet(styles)((props) => {
  const {classes} = props;
  return (
    <div className={classes.row}>
      <Header classes={classes}/>
      {/* <!-- content --> */}
      <main>
        {/* <!-- left side --> */}
        <LeftSide classes={classes}/>
        {/* <!-- Center --> */}
        <Center classes={classes}/>
        {/* <!-- right side --> */}
        <RightSide classes={classes}/>
      </main>
    </div>
  );
})
export default Homepage
