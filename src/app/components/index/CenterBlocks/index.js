import React, {useState, useEffect} from "react"
import {
  Item1,
  likeComment,
  Bitmap,
  Avatar,
  Bitmap3,
  Comment
} from "../../../Utils/Images"
import {useAccordion} from "../../../Utils/Hooks"

/**
 * Direct
*/
function Direct(props) {
  const [open, dispatch] = useAccordion();
  const [item, setItem] = useState(1);
  const {classes} = props

  function handleAccordion(stat, item) {
    dispatch(stat);
    setItem(item)
  }

  return (<dl className={[classes.accordion, classes.collapsable].join(" ")}>
    <dt onClick={() => handleAccordion(!open, open?0:1)} className={[
        item === 1
          ? classes.expanded
          : "",
        classes.close
      ].join(" ")}>
      <span className="icon_link"></span>
      <span className={classes.small}>
        Direct naar
      </span>
  </dt>
    <dd>
      <div className={classes.direct}>
        <div className={classes.item}>
          <div className="col3">
            <img src={Item1} title="item1"/>
          </div>
          <div className="col9">
            <span>YouForce</span>
          </div>
        </div>

        <div className={classes.item}>
          <div className="col3">
            <img src={Bitmap} title="item2"/>
          </div>
          <div className="col9">
            <span>MIP-meldingen</span>
          </div>
        </div>

        <div className={classes.item}>
          <div className="col3">
            <img src={Bitmap3} title="item3"/>
          </div>
          <div className="col9">
            <span>Topdesk
            </span>
          </div>
        </div>

      </div>
    </dd>
  </dl>)
}

/**
 * Blogs
*/
function Blogs(props) {
  const [open, dispatch] = useAccordion();
  const [item, setItem] = useState(1);
  const {classes} = props

  function handleAccordion(stat, item) {
    dispatch(stat);
    setItem(item)
  }

  return (<dl className={[classes.accordion, classes.collapsable].join(" ")}>
    <dt onClick={() => handleAccordion(!open,  open?0:1)} className={[
        item === 1
          ? classes.expanded
          : "",
        classes.close
      ].join(" ")}>
      <span className="icon_edit"></span>BLOGS</dt>
    <dd className="blogs">
      <div className="col">
        <div className="col3">
          <img src={Avatar} className="avatar"/>
        </div>
        <div className="col4">
          <span className={classes.author}>Jacob Cobijn</span>
          <br/>
          <span className={classes.date}>12/04/2016</span>
        </div>
        <div className={['col5', classes.like_comment].join(" ")}>
          <div className="col">
            <img src={likeComment}/>
            <img src={Comment}/>
          </div>
        </div>
      </div>
      <div className={classes.blog_article}>
        <div className={classes.blog_title}>
          Samenleving
        </div>
        <div className={classes.blog_content}>
          Het hebben van een partner die positief in het leven staat, is mogelijk goed voor de….
        </div>
      </div>
    </dd>

    <dt className={classes.meer}>Meer Blogs</dt>
    <dd></dd>
  </dl>)
}

/**
 * Kwaliteitshandboek
*/
function Kwaliteitshandboek(props) {
  const [open, dispatch] = useAccordion();
  const [item, setItem] = useState(1);
  const {classes} = props

  function handleAccordion(stat, item) {
    dispatch(stat);
    setItem(item)
  }

  return (<dl className={[classes.accordion, classes.collapsable].join(" ")}>
    <dt onClick={() => handleAccordion(!open,  open?0:1)} className={[
        item === 1
          ? classes.expanded
          : "",
        classes.close
      ].join(" ")}>
      <span className="icon_static"></span>
      <small className={classes.small}>Kwaliteitshandboek</small>
    </dt>
    <dd>
      <div className={classes.Kwaliteits}>
        <div className={classes.item}>
          <div>
            <span>Klachtenprocedure
            </span>
            <span>bijgewerk / 18-04-2016
            </span>
          </div>
          <div>
            <span>Periodieke test gebruikersgroepen
            </span>
            <span>bijgewerk / 10-02-2016
            </span>
          </div>
          <div>
            <span>Dienstrichtlijn tweede niveau
            </span>
            <span>bijgewerk / 09-05-2015
            </span>
          </div>
        </div>
      </div>
    </dd>

    <dt className={classes.meer}>Naar Kwaliteitshandboek</dt>
    <dd></dd>
  </dl>)
}

/**
 * Peilingen
*/
function Peilingen(props) {
  const {classes} = props

  return (<dl className={[classes.accordion, classes.collapsable].join(" ")}>
    <dt className={[classes.expanded, classes.close].join(" ")}>
      <span className="icon_static"></span>Peilingen</dt>
    <dd className={classes.paddingLg}>
      Er zijn geen peilingen beschikbaar.
    </dd>
  </dl>)
}

/**
 * Peilingen
*/
function MijnLinks(props) {
  const [open, dispatch] = useAccordion();
  const [item, setItem] = useState(1);
  const {classes} = props

  function handleAccordion(stat, item) {
    dispatch(stat);
    setItem(item)
  }

  return (<dl className={[classes.accordion, classes.collapsable].join(" ")}>
    <dt onClick={() => handleAccordion(!open, open?0:1)} className={[
        item === 1
          ? classes.expanded
          : "",
        classes.close
      ].join(" ")}>
      <span className="icon_link"></span>Mijn links</dt>
    <dd>
      <div className={classes.MijnLinks}>
        <div className={classes.item}>
          <div>
            <span>Zorggroep.nl</span>
          </div>
          <div>
            <span>zorgvisie.nl
            </span>
          </div>
          <div>
            <span>medicalfacts.nl</span>
          </div>
        </div>
      </div>
    </dd>
  </dl>)
}

export {
  Direct,
  Blogs,
  Kwaliteitshandboek,
  Peilingen,
  MijnLinks
}
