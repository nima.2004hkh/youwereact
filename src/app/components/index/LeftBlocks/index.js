import React, {useState, useEffect} from "react"
import {Rectangle, likeComment, Rectangle5, Rectangle6} from "../../../Utils/Images"
import {useAccordion} from "../../../Utils/Hooks"

/**
 * Accordions
*/
function Accordions(props) {
  const [open, dispatch] = useAccordion();
  const [item, setItem] = useState(2);
  const {classes} = props

  function handleAccordion(stat, item) {
    dispatch(stat);
    setItem(item)
  }

  return (<dl className={[classes.accordion, classes.collapsable, classes.accordions].join(" ")}>

    <dt className={item === 1
        ? classes.expanded
        : ""} onClick={() => handleAccordion(!open, 1)}>Accordion 1</dt>
    <dd>
      <p className={classes.inner_content}>
        Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis
      </p>
    </dd>

    <dt className={item === 2
        ? classes.expanded
        : ""} onClick={() => handleAccordion(!open, 2)}>Accordion 2</dt>
    <dd>
      <p className={classes.inner_content}>
        Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis
      </p>
    </dd>

    <dt className={item === 3
        ? classes.expanded
        : ""} onClick={() => handleAccordion(!open, 3)}>Accordion 3</dt>
    <dd>
      <p className={classes.inner_content}>
        Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis
      </p>
    </dd>
  </dl>)
}

/**
 * Evenementen
*/
function Evenementen(props) {
  const [open, dispatch] = useAccordion();
  const [item, setItem] = useState(1);
  const {classes} = props

  function handleAccordion(stat, item) {
    dispatch(stat);
    setItem(item)
  }

  return (<dl className={[classes.accordion, classes.collapsable].join(" ")}>
    <dt onClick={() => handleAccordion(!open, 1)} className={[
        item === 1
          ? classes.expanded
          : "",
        classes.close
      ].join(" ")}>
      <span className="icon_calendar"></span>
      <span className={classes.small}>
        Evenementen
      </span>
    </dt>
    <dd>
      <div className={classes.list_view}>
        <div>
          <span>
            10/12/2016
          </span>
          <span className={classes.title}>Zorgmarathon</span>
        </div>
        <div>
          <span>
            12/12/2016
          </span>
          <span className={classes.title}>Beurs</span>
        </div>
        <div>
          <span>
            30/12/2016
          </span>
          <span className={classes.title}>Onze zorgevenement</span>
        </div>
      </div>
    </dd>

    <dt onClick={() => handleAccordion(!open, 2)} className={[
        item === 2
          ? classes.expanded
          : "",
        classes.meer
      ].join(" ")}>Meer Evenementen</dt>
    <dd>
      <div className={classes.list_view}>
        <div>
          <span>
            10/12/2016
          </span>
          <span className={classes.title}>Zorgmarathon</span>
        </div>
        <div>
          <span>
            12/12/2016
          </span>
          <span className={classes.title}>Beurs</span>
        </div>
        <div>
          <span>
            30/12/2016
          </span>
          <span className={classes.title}>Onze zorgevenement</span>
        </div>
      </div>
    </dd>
  </dl>)
}

/**
 * Nieuws
*/
function Nieuws(props) {
  const [open, dispatch] = useAccordion();
  const [item, setItem] = useState(1);
  const {classes} = props

  function handleAccordion(stat, item) {
    dispatch(stat);
    setItem(item)
  }

  return (<dl className={[classes.accordion, classes.collapsable].join(" ")}>
    <dt
      onClick={() => handleAccordion(!open, open?0:1)}
      className={[item===1 ? classes.expanded : "", classes.close].join(" ")}>
      <span className="icon_news"></span>Nieuws</dt>
    <dd>
      <div className={classes.articles}>
        <div className={classes.item}>
          <div className="col5">
            <img src={Rectangle} title="item1"/>
          </div>
          <div>
            <span>29/02/2016</span>
            <span>Geef je mening over ons intranet</span>
          </div>
          <div className={[classes.like, 'col2'].join(" ")}>
            <img src={likeComment} title="item1"/>
          </div>
        </div>

        <div className={classes.item}>
          <div className="col5">
            <img src={Rectangle5} title="item2"/>
          </div>
          <div>
            <span>29/02/2016</span>
            <span>Commissiebrief over nieuwe Wlz
            </span>
          </div>
          <div className={[classes.like, 'col2'].join(" ")}>
            <img src={likeComment} title="item1"/>
          </div>
        </div>

        <div className={classes.item}>
          <div className="col5">
            <img src={Rectangle6} title="item3"/>
          </div>
          <div>
            <span>29/02/2016</span>
            <span>VIO erkend als theoretisch goed
            </span>
          </div>
          <div className={[classes.like, 'col2'].join(" ")}>
            <img src={likeComment} title="item1"/>
          </div>
        </div>
      </div>
    </dd>

    <dt className={[classes.meer].join(" ")}>Meer Nieuws</dt>
    <dd></dd>

  </dl>)
}

/**
 * Widget
*/
function Widget(props) {
  const {classes} = props

  return (<dl className={[classes.accordion, classes.collapsable, classes.no_icon].join(" ")}>
    <dt className={[classes.meer, classes.plus].join(" ")}>Widget toevoegen</dt>
    <dd></dd>
  </dl>)
}

export {
  Accordions,
  Nieuws,
  Widget,
  Evenementen
}
