import React from "react"
import {Direct, Blogs, Kwaliteitshandboek, Peilingen, MijnLinks} from "./CenterBlocks"

export default function Center(props){
  const {classes} = props;
  const BlocksNames = ['Direct', 'Blogs', 'Kwaliteitshandboek', 'Peilingen', 'MijnLinks'];
  const Blocks = {
    Direct: Direct,
    Blogs: Blogs,
    Kwaliteitshandboek: Kwaliteitshandboek,
    Peilingen: Peilingen,
    MijnLinks: MijnLinks,
  };

  return(
    <div className={[classes.column,classes.block].join(" ")}>
      {
        BlocksNames.map(component =>{
          const Handler = Blocks[component];
          return (<Handler classes={classes} key={component}/>)
        })
      }
    </div>
  )
}
