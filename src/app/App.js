import React from 'react';
import Homepage from "./components/index"
import {ThemeProvider} from "react-jss"
import {theme} from "./ThemeProviderConfig"

function App () {
        return (
          <ThemeProvider theme={theme}>
            <div>
               <Homepage/>
            </div>
          </ThemeProvider>
        );
}

export default App
