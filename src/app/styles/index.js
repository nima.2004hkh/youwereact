import {
  Bitmap,
  accordionShape,
  ArrowRight,
  ArrowRightSecondary,
  Zoeken,
  addImage,
  phonebook,
  Groups,
  Kwaliteitshandboek,
  Shape,
  ExternalLink,
  Calendar,
  newsPaper
} from "../Utils/Images"

export const index = theme => ({
  '@media screen and (min-width:300px) and (max-width:990px)': {
    accordion: {
      '& dt': {
        fontSize: '3vw !important'
      }
    },
    close: {
      '&:before': {
        padding: '12px !important',
        paddingBottom: '16px !important',
        top: '-16px !important',
        left: '16px !important'
      }
    },
    small: {
      fontSize: '0.71rem !important',
      width: 87,
      fontWeight: 'bold',
      display: 'inline-block',
      whiteSpace: 'nowrap',
      overflow: 'hidden !important',
      textOverflow: 'ellipsis'
    }
  },
  '@media screen and (min-width:1200px) and (max-width:1300px)': {
    small: {
      fontSize: '0.71rem !important',
      width: 104,
      fontWeight: 'bold',
      display: 'inline-block',
      whiteSpace: 'nowrap',
      overflow: 'hidden !important',
      textOverflow: 'ellipsis'
    }
  },
  '@media screen and (max-width: 1200px) and (min-width: 1000px)': {
    expanded: {
      '&:after': {
        padding: '15px !important'
      }
    },
    close: {
      '&:before': {
        padding: '12px !important'
      }
    },
    accordion: {
      '& dt': {
        fontSize: '1.2vw !important'
      }
    }
  },
  '@media screen and (max-width: 1100px) and (min-width: 1000px)': {
    small: {
      fontSize: '0.71rem !important',
      width: 87,
      fontWeight: 'bold',
      display: 'inline-block',
      whiteSpace: 'nowrap',
      overflow: 'hidden !important',
      textOverflow: 'ellipsis'
    },
    expanded: {
      '&:after': {
        padding: '15px !important'
      }
    },
    close: {
      '&:before': {
        padding: '12px !important'
      }
    }
  },
  '@media screen and (max-width: 1000px)': {
    small: {
      fontSize: '0.71rem !important'
    },
    column: {
      width: '100% !important'
    },
    accordions: {
      '& expanded:after': {
        padding: '17px !important'
      }
    },
    accordion: {
      '& dt': {
        fontSize: '0.94em !important',
        textTransform: 'capitalize !important'
      }
    }
  },

  '@media screen and (min-width: 1920px)': {
    expanded: {
      '&:after': {
        padding: '17px !important'
      }
    },
    close: {
      '&:before': {
        padding: '17px !important'
      }
    }
  },
  '@keyframes openAccordion': {
    from: {
      opacity: '0',
      height: 0
    },
    to: {
      opacity: '1',
      height: '100%'
    }
  },
  '@global': {
    main: {
      paddingRight: theme.paddingUnit,
      paddingLeft: theme.paddingUnit
    },
    '*': {
      boxSizing: 'border-box',
      margin: 0,
      padding: 0,
      fontFamily: 'Open Sans'
    },
    '#site_name': {
      fontSize: theme.fontSizeHeadUnit,
      fontWeight: 'bold',
      marginRight: 0
    },
    '#slogan_text': {
      display: 'block',
      marginTop: -10
    },
    '#slogan_bold': {
      fontWeight: 'bold',
      marginBottom: 'auto',
      position: 'relative',
      textAlign: 'right',
      marginLeft: 10,
      fontSize: 24
    },
    '#site_name_part_two': {
      fontSize: theme.fontSizeHeadUnit
    },
    '.search': {
      paddingRight: '7%',
      marginTop: 15
    },
    "span[class^='icon_'], span[class*=' icon_']": {
      paddingRight: 30,
      paddingLeft: 30,
      paddingBottom: 32,
      backgroundColor: '#E5F0FA',
      backgroundPosition: 'center',
      position: 'relative',
      top: -16,
      left: -16
    },
    '.icon_news': {
      background: `url(${newsPaper}) no-repeat`
    },
    '.icon_calendar': {
      background: `url(${Calendar}) no-repeat`
    },
    '.icon_link': {
      background: `url(${ExternalLink}) no-repeat`
    },
    '.icon_edit': {
      background: `url(${Shape}) no-repeat`
    },
    '.icon_static': {
      background: `url(${Kwaliteitshandboek}) no-repeat`
    },
    '.icon_groups': {
      background: `url(${Groups}) no-repeat`
    },

    /* columns */
    '.col5': {
      width: 'calc((100 * 5%) / 12)'
    },
    '.col3': {
      width: 'calc((100 * 3%) / 12)'
    },
    '.col4': {
      width: 'calc((100 * 4%) / 12)'
    },
    '.col7': {
      width: 'calc((100 * 7%) / 12)'
    },
    '.col8': {
      width: 'calc((100 * 8%) / 12)'
    },
    '.col9': {
      width: 'calc((100 * 9%) / 12)'
    },
    '.col': {
      width: '100%',
      display: 'flex'
    },
    'dd p.inner_content': {
      fontSize: 14,
      marginBottom: 20,
      padding: 10
    }
  },
  small: {},
  row: {
    '&:after': {
      content: '""',
      display: 'table',
      clear: 'both'
    }
  },
  wrapper: {
    padding: '3%'
  },
  column: {
    float: 'left',
    width: ' 33.33%',
    padding: 10,
    height: 'auto',
    flex: '1 0 calc(33.333% - 20px)',
    padding: '20px 0px 20px 15px'
  },
  /* Clear floats after the columns */
  row: {
    '&:after': {
      content: '""',
      display: 'table',
      clear: 'both'
    }
  },
  grids: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    height: 'max-content',
    flexFlow: 'row wrap',
    counterReset: 'brick'
  },
  fullwidth: {
    width: '100%'
  },
  navigation: {
    background: theme.mainHeaderColor,
    paddingRight: theme.paddingUnit,
    paddingLeft: theme.paddingUnit,
    '& $search': {
      paddingRight: '7%',
      marginTop: 15
    }
  },

  /* top Header */
  logo: {
    display: 'flex',
    paddingLeft: theme.paddingUnit,
    paddingBottom: 20,
    paddingTop: 20
  },
  image: {
    background: `url(${Bitmap})`,
    height: 51,
    width: 49,
    backgroundRepeat: 'no-repeat',
    marginRight: '1%'
  },
  slogan: {
    position: 'relative',
    top: '50%'
  },
  nav: {
    listStyleType: 'none',
    margin: 0,
    padding: 0,
    overflow: 'hidden',
    '& > li': {
      float: 'left',
      '& > a': {
        display: 'block',
        color: 'white',
        textAlign: 'center',
        padding: '24px 16px',
        textDecoration: 'none',
        fontSize: 16,
        fontFamily: 'Open Sans',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        '&:hover': {
          backgroundColor: '#892786'
        },
        '&:active': {
          background: 'unset',
          '&:after': {
            content: 'unset'
          }
        }
      }
    }
  },
  /* appbar */
  appbar: {
    marginTop: 5,
    paddingRight: theme.paddingUnit,
    paddingLeft: theme.paddingUnit,
    '& #appbar_wrapper': {
      borderBottom: '1px solid #eee',
      display: 'flex'
    },
    '& #left_menu': {
      display: 'flex',
      flex: 'auto'
    }
  },
  buttons: {
    background: theme.buttonBackground,
    padding: 12,
    width: 50,
    height: 50,
    marginLeft: 5
  },
  top_menu: {
    listStyleType: 'none',
    margin: 0,
    padding: 0,
    overflow: 'hidden',
    '& > li': {
      float: 'left',
      '& > a': {
        display: 'block',
        color: '#657189',
        textAlign: 'center',
        padding: '14px 16px',
        textDecoration: 'none',
        fontSize: 14,
        fontWeight: 'normal',
        fontFamily: 'Open Sans',
        fontWeight: 'bold',
        '&:hover': {
          color: '#892786',
          background: 'unset'
        },
        '& .active': {
          background: 'unset',
          '&:after': {
            content: 'unset'
          }
        }
      }
    }
  },
  /* search */
  search: {
    marginTop: 30,
    marginBottom: 10
  },
  search_input: {
    border: 'unset',
    height: 40,
    width: '100%',
    paddingLeft: 10,
    paddingRight: '15%',
    background: `url(${Zoeken}) no-repeat scroll #fff`,
    backgroundSize: 'auto',
    backgroundPosition: 'center right',
    backgroundPositionX: '95%'
  },
  /* accordion */
  active: {
    '&:after': {
      content: `url(${ArrowRightSecondary})`
    }
  },
  panel: {
    padding: ' 0 18px',
    backgroundColor: 'white',
    maxHeight: 0,
    overflow: 'hidden',
    transition: 'max-height 0.2s ease-out'
  },

  /* accordion */
  block: {
    '& dl': {
      padding: 0
    }
  },
  no_icon: {
    '& dt': {
      '&:after': {
        content: 'unset !important'
      }
    }
  },
  accordions: {
    '& $expanded:after': {
      padding: '17px !important',
      marginTop: ' -17 !important',
      marginRight: '-17px !important',
      background: '#E5F0FA !important'
    },
    '& $expanded': {
      background: '#657189 !important'
    }
  },
  /* close Icon */
  accordion: {
    marginBottom: 15,
    '& dt': {
      color: '#FF952D',
      fontWeight: 'bold',
      transition: '0s',
      opacity: 1,
      padding: 16,
      background: '#EBF0F4',
      border: '1px solid #d8d8d8',
      borderBottomColor: 'rgb(216, 216, 216)',
      borderBottomStyle: 'solid',
      borderBottomWidth: 0,
      userSelect: 'none',
      cursor: 'pointer',
      transition: '0.1s',
      // fontSize: '0.8vw',
      textTransform: 'uppercase',
      '&:after': {
        content: `url(${ArrowRight})`,
        //background: '#E5F0FA',
        float: 'right',
        padding: 13,
        position: 'relative',
        top: -17,
        left: 17,
        width: 21
      }
    },
    '& $plus': {
      '&:after': {
        content: '""'
      },
      '&:before': {
        content: `url(${addImage})`,
        // background: '#E5F0FA',
        float: 'right',
        padding: 11,
        position: 'relative',
        top: -16,
        left: 15,
        width: 26,
        textAlign: 'center'
      }
    },
    '& dd': {
      display: 'none',
      border: '1px solid #EAEAEA',
      borderTopColor: 'rgb(234, 234, 234)',
      borderTopStyle: 'solid',
      borderTopWidth: 0,
      padding: 10,
      margin: 0,
      fontSize: '0.8rem'
    },
    '& $expanded': {
      '&:after': {
        content: `url(${ArrowRightSecondary})`,
        float: 'right',
        position: 'unset',
        top: 'unset',
        left: 'unset',
        padding: 15,
        marginTop: -17,
        marginRight: -17,
        //  background: '#E5F0FA'
      },
      '& + dd': {
        display: 'block',
        animation: '$openAccordion 1000ms ease-in'
        // animationName: '$openAccordion',
        // animationDuration: '4s',
      },
      background: 'unset ',
      opacity: 1,
      fontFamily: 'Open Sans',
      fontWeight: 'bold'
    }
  },
  close: {
    '&:before': {
      content: `url(${accordionShape})`,
      background: '#E5F0FA',
      float: 'right',
      padding: 14,
      position: 'relative',
      top: -17,
      left: 17,
      width: 21
    }
  },
  expanded: {
    '& i': {
      transform: 'rotate(90deg)',
      opacity: 1
    },
    '&:after': {
      content: `url(${ArrowRightSecondary})`,
      float: 'right',
      padding: 'unset',
      position: 'unset',
      top: 'unset',
      left: 'unset',
      background: 'unset'
    }
  },
  plus: {},

  /* microblog */
  microblog: {
    '& form textarea': {
      width: '100%'
    },
    '& .tools .plaats': {
      color: '#fff',
      padding: 10,
      float: 'right',
      marginTop: 'unset',
      '& button': {
        background: '#44B9F6',
        padding: 5,
        border: 0,
        color: '#fff',
        width: 100,
        cursor: 'pointer'
      }
    }
  },
  under_content: {
    fontFamily: 'Open Sans',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    display: 'flex',
    alignItems: 'flex-end',
    color: '#657189'
  },

  /* blogs */
  author: {
    fontSize: '0.7rem',
    fontWeight: 'bold',
    marginLeft: 10,
    color: '#333333'
  },
  date: {
    fontSize: 14,
    marginLeft: 10,
    color: '#b7b7b7'
  },
  like_comment: {
    '& .col': {
      display: 'block',
      textAlign: 'right'
    }
  },
  blog_article: {
    paddingLeft: '2%',
    paddingTop: 20
  },
  blog_content: {
    fontSize: 14,
    paddingTop: 15,
    paddingBottom: 15,
    color: '#b7b7b7'
  },
  blog_title: {
    paddingTop: 10,
    fontSize: 15,
    fontWeight: 'bold'
  },
  inner_content: {
    fontSize: 14,
    marginBottom: 20,
    padding: 10
  },

  /* Direct */
  direct: {
    display: 'block',
    '& $item span': {
      fontSize: 12,
      fontWeight: 'bold',
      display: 'inline-block',
      color: '#333333'
    },
    '& $item:last-child': {
      borderBottom: 'none'
    },
    '& $item': {
      display: 'flex',
      height: 65,
      borderBottom: '1px solid #eee',
      width: '100%',
      position: 'relative'
    },
    '& $item div': {
      display: 'inline-block',
      height: '100%',
      margin: 0,
      padding: 0,
      paddingTop: 10
    },
    '& $item div:first-child': {
      textAlign: 'center'
    },
    '& $item .col9 span': {
      marginLeft: '20%',
      marginTop: '5%'
    }
  },
  title: {},
  item: {},
  /* List view */
  list_view: {
    padding: theme.paddingUnitSm,
    '& div': {
      borderBottom: '1px solid #eee',
      paddingBottom: 20,
      paddingTop: 20,
      '& $title': {
        fontSize: '0.7rem',
        marginLeft: 30,
        color: '#333'
      },
      '& span': {
        borderBottom: '1 px solid #eee',
        paddingBottom: 20,
        paddingTop: 20,
        fontSize: 14,
        marginLeft: 10,
        color: '#b7b7b7'
      },
      '&:last-child , &:last-child span': {
        borderBottom: 'none'
      }
    }
  },
  /* articles */
  articles: {
    display: 'block',
    '& $item': {
      display: 'flex',
      height: 110,
      borderBottom: '1px solid #eee',
      width: '100%',
      position: 'relative',
      paddingTop: 15,
      '& div': {
        display: 'inline-block',
        height: '100%',
        margin: 0,
        padding: 0,
        '&.like img': {
          float: 'right'
        }
      },
      '& span': {
        fontSize: 13,
        display: 'inline-block',
        marginTop: 10,
        color: '#b7b7b7',
        padding: 5,
        '&:last-child': {
          fontSize: 12,
          fontWeight: 'bold',
          display: 'inline-block',
          color: '#333333'
        }
      },
      '&:last-child': {
        borderBottom: 'none'
      },
      '&.like ': {
        position: 'absolute',
        left: '86%'
      }
    }
  },

  /* Kwaliteits */
  Kwaliteits: {
    padding: 5,
    '& $item': {
      '& div': {
        width: '100%',
        paddingBottom: 10,
        paddingTop: 10,
        fontSize: 14,
        display: 'grid'
      },
      '& div:not(:last-child)': {
        borderBottom: '1px solid #eee'
      },
      '& span:first-child': {
        fontSize: 12,
        fontWeight: 'bold',
        display: 'inline-block',
        color: '#333333'
      },
      '& span:last-child': {
        fontSize: 13,
        display: 'inline-block',
        marginTop: 10,
        color: '#b7b7b7',
        padding: '5px 0px 5px 0px',
        fontStyle: 'italic'
      }
    }
  },

  /* MijnLinks */
  MijnLinks: {
    padding: 5,
    '& $item': {
      '& div:not(:last-child)': {
        borderBottom: '1px solid #eee'
      },
      '& div': {
        width: '100%',
        paddingBottom: 20,
        paddingTop: 20,
        fontSize: 14,
        display: 'grid',
        '& span:first-child': {
          fontSize: 12,
          fontWeight: 'bold',
          display: 'inline-block',
          color: '#333333'
        }
      }
    }
  },

  /* phoneBook */
  phoneBook: {
    backgroundImage: `url(${phonebook})`,
    backgroundColor: '#00C4E6',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center right',
    padding: 20,
    marginBottom: 15,
    '& h3': {
      color: '#ffffff',
      fontFamily: 'Open Sans',
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: 18,
      display: 'flex',
      alignItems: 'flex-end',
      textTransform: 'uppercase'
    },
    '& .text-white': {
      color: '#ffffff',
      fontFamily: 'Open Sans',
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: 12,
      /* identical to box height */
      display: 'flex',
      alignItems: 'flex-end'
    }
  },
  groups: {
    '& $item': {
      height: 'unset',
      padding: '5px 0px 5px 0px'
    }
  },
  meer: {
    fontSize: 14,
    color: '#657189 !important',
    fontWeight: 'normal !important'
  }

})
