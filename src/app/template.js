export default({markup, sheets}) => {
  return `<!DOCTYPE html>
         <html>
            <head>
              <meta charset="utf-8" />
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
              <style type="text/css">${sheets.toString()}</style>
            </head>
            <body>
               <div id="root">${markup}</div>
               <script src="/dist/client.js" async></script>
            </body>
         </html>`;
};
