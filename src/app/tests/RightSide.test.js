import React from 'react';
import {mount} from '../../../enzyme';
import RightSide from "../components/index/RightSide"
import {ThemeProvider} from "react-jss"
import {theme} from "../ThemeProviderConfig"
import {index as styles} from "../styles"
import injectSheet from 'react-jss';

const InjectStyle = injectSheet(styles)((props) => {
  const {classes} = props;
  return (<RightSide classes={classes}/>)
});

const Wrapper = mount(<ThemeProvider theme={theme}>
  <InjectStyle/>
</ThemeProvider>);

test('Count Heads', () => {
  expect(Wrapper.find('dt')).toHaveLength(4)
});

test('Check Telefoonboek exists', () => {
  expect(Wrapper.contains('Telefoonboek')).toBeTruthy()
});
