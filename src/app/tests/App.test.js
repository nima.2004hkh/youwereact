import React from 'react';
import renderer from 'react-test-renderer';
import App from "../App"

test('Component is Rendered successfully', () => {
  const component =  renderer.create(<App/>)
  const tree =component.toJSON();
  //check that App is rednered successfully
  expect(tree).toMatchSnapshot();
});
