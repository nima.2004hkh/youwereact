import React from 'react';
import {mount} from '../../../enzyme';
import Header from "../components/Header"
import {ThemeProvider} from "react-jss"
import {theme} from "../ThemeProviderConfig"
import {index as styles} from "../styles"
import injectSheet from 'react-jss';

const InjectStyle = injectSheet(styles)((props) => {
  const {classes} = props;
  return (<Header classes={classes}/>)
});
const Wrapper = mount(<ThemeProvider theme={theme}>
  <InjectStyle/>
</ThemeProvider>);

test('Count Header Uls', () => {
  //check that App is rednered successfully
  expect(Wrapper.find('ul')).toHaveLength(3); // top left menu , top right menu , app menu
});

test('Count Header Lis', () => {
  expect(Wrapper.find('li')).toHaveLength(10);
})
