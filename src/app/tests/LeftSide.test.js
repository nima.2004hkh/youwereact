import React from 'react';
import {mount} from '../../../enzyme';
import LeftSide from "../components/index/LeftSide"
import {ThemeProvider} from "react-jss"
import {theme} from "../ThemeProviderConfig"
import {index as styles} from "../styles"
import injectSheet from 'react-jss';

const InjectStyle = injectSheet(styles)((props) => {
  const {classes} = props;
  return (<LeftSide classes={classes}/>)
});
const Wrapper = mount(<ThemeProvider theme={theme}>
  <InjectStyle/>
</ThemeProvider>);

test('Count Heads', () => {
  expect(Wrapper.find('dt')).toHaveLength(8)
});
