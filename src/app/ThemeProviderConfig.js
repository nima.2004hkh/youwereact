const spaceUnit = '1em';

export const theme = {
  padding: '10%',
  mainHeaderColor: '#9E419C',
  buttonBackground: '#E5F0FA',
  paddingUnit: '10%',
  paddingUnitMd: '5%',
  paddingUnitSm: '2%',
  fontSizeHeadUnit: 24,
  spaceXxs: `calc(0.25 * ${spaceUnit})`,
  spaceXs: `calc(0.5 * ${spaceUnit})`,
  spaceSm: `calc(0.75 * ${spaceUnit})`,
  spaceMd: `calc(1.25 * ${spaceUnit})`,
  spaceLg: `calc(2 * ${spaceUnit})`,
  spaceXl: `calc(3.25 * ${spaceUnit})`,
  spaceXxl: `calc(5.25 * ${spaceUnit})`
}
