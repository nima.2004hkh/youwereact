import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Template from './app/template';
import App from './app/App';
import {JssProvider, SheetsRegistry, createGenerateId} from 'react-jss'

export default function serverRenderer({clientStats, serverStats}) {
  return(req, res, next) => {

    const sheets = new SheetsRegistry();
    const generateId = createGenerateId();
    const markup = ReactDOMServer.renderToString(
      <JssProvider registry={sheets} generateId={generateId}>
        <App/>
    </JssProvider>);

    res.status(200).send(renderFullPage(markup, sheets));
  };

  function renderFullPage(markup, sheets) {
    return `<html>
            <head>
               <style type="text/css">${sheets.toString()}</style>
            </head>
            <body>
               <div id="root">${markup}</div>
               <script src="/dist/client.js" async></script>
            </body>
         </html>
    `
  }
}
